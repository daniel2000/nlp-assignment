from Generation.generateSentence import SentenceGenerator
from Grams.get_generateLMs import *
from Probability.TestingFilesProbability import *

corpusDirectory= "corpusTexts"
languageModelDirectory="languageModels22.03.2020--19.05.10"

#getLM = get_generateLMs(corpusDirectory=corpusDirectory, trainingPercentage=80)
getLM = get_generateLMs(languageModelDirectory=languageModelDirectory)

def chooseNGram():
    # Giving the user to option to choose between a unigram or bigram or trigram
    choice = 0
    while choice != -1:
        print("Please choose between")
        print("1. Unigram")
        print("2. Bigram")
        print("3. Trigram")
        print("4. Exit")
        choice = int(input("Choice (number): "))
        if choice == 1:
            return 1
        elif choice == 2:
            return 2
        elif choice == 3:
            return 3
        elif choice == 4:
            return -1
        else:
            print("Incorrect Input")



def chooseALanguageModel():
    # When the user chooses to select a language model
    currentLanguageModel = None
    currentN = 0
    choice = -1
    version = None

    while choice == -1:
        # The user is given a choice between the 4 versions
        print("Please choose flavour of the model")
        print("1. Vanilla Version")
        print("2. Laplace Version")
        print("3. UNK Version")
        print("4. Linear Interpolation")
        print("5. End")
        choice = int(input("Choice (number): "))

        # Vanilla Version
        if choice == 1:
            # Asking the user to choose between unigram, bigram, trigram
            # According to the input, the respective ngram is generated and set to the variable currentLanguageModel
            gramChoice = chooseNGram()
            currentN = gramChoice
            if gramChoice == -1:
                continue
            elif gramChoice == 1:
                currentLanguageModel = getLM.getVanillaUnigram()
            elif gramChoice == 2:
                currentLanguageModel = getLM.getVanillaBigram()
            elif gramChoice == 3:
                currentLanguageModel = getLM.getVanillaTrigram()

            # Checking if the language model generated is empty.
            if not checkGram(currentLanguageModel):
                continue
            else:
                print("Language Model generated Successfully")
                version = "vanilla"
        elif choice == 2:
            # Laplace Version
            # Asking the user to choose between unigram, bigram, trigram
            # According to the input, the respective ngram is generated and set to the variable currentLanguageModel
            gramChoice = chooseNGram()
            currentN = gramChoice
            if gramChoice == -1:
                continue
            elif gramChoice == 1:
                currentLanguageModel = getLM.getLaplaceUnigram()
            elif gramChoice == 2:
                currentLanguageModel = getLM.getLaplaceBigram()
            elif gramChoice == 3:
                currentLanguageModel = getLM.getLaplaceTrigram()

            # Checking if the language model generated is empty.
            if not checkGram(currentLanguageModel):
                continue
            else:
                print("Language Model generated Successfully")
                version = "laplace"

        elif choice == 3:
            # UNK Version
            # Asking the user to choose between unigram, bigram, trigram
            # According to the input, the respective ngram is generated and set to the variable currentLanguageModel
            gramChoice = chooseNGram()
            currentN = gramChoice
            if gramChoice == -1:
                continue
            elif gramChoice == 1:
                currentLanguageModel = getLM.getUnkUnigram()
            elif gramChoice == 2:
                currentLanguageModel = getLM.getUnkBigram()
            elif gramChoice == 3:
                currentLanguageModel = getLM.getUnkTrigram()

            # Checking if the language model generated is empty.
            if not checkGram(currentLanguageModel):
                continue
            else:
                print("Language Model generated Successfully")
                version = "unk"

        elif choice == 4:
            choice2 = -1
            # Linear Interpolation
            # Asking the user to choose between vanilla, laplace, unk
            # According to the input, the respective ngram is generated and set to the variable currentLanguageModel
            while choice2 == -1:
                print("Please choose flavour of linear interpolation")
                print("1. Vanilla Version")
                print("2. Laplace Version")
                print("3. UNK Version")
                print("4. End")
                choice2 = int(input("Choice: "))

                if choice2 == 1:
                    currentLanguageModel = getLM.getLinearInterpolation("vanilla")
                    currentN = 3
                    version = "vanilla"
                elif choice2 == 2:
                    currentLanguageModel = getLM.getLinearInterpolation("laplace")
                    currentN = 3
                    version = "laplace"

                elif choice2 == 3:
                    currentLanguageModel = getLM.getLinearInterpolation("unk")
                    currentN = 3
                    version = "unk"

                elif choice2 == 4:
                    break
                else:
                    print("Incorrect Input")

                # Checking if the language model generated is empty.
                if not checkGram(currentLanguageModel):
                    continue
                else:
                    print("Language Model generated Successfully")
        elif choice == 5:
            break
        else:
            print("Incorrect Input")
    return currentLanguageModel, currentN, version





sequenceProbability = SequenceProbability()
sentenceGenerator = SentenceGenerator(getLM)

langaugeModel = None
n = 0
version = None

while True:
    # Asking the user to choose form the below options
    print("Please choose from the below options")
    print("1. Select Language Model")
    print("2. Generate a sequence")
    print("3. Calculate the probability of a sequence")
    print("4. Calculate the probability of all sentences in the test files")
    print("5. End")
    choice = int(input("Choice (number): "))

    if choice == 1:
        # If the user wants to select a language model, the method is called
        langaugeModel, n, version = chooseALanguageModel()
    elif choice == 2:
        # Making sure that the selected language Model is not empty
        if not checkGram(langaugeModel,n=n):
            continue

        # Asking the user to enter a number of minimum and maximum words
        print("Please enter minimum no of words")
        min = int(input("Minimum no of words: "))
        print("Please enter maximum no of words, enter 0 for no maximum")
        max = int(input("maximum no of words: "))
        if max == 0:
            max = None
        # Asking the user to input the sequence
        print("Please insert starting sequence")
        sequence = input("Sequence: ")

        # Given the langauge model chosen above, the appropriate sentence generator is called
        if n == 1:
            print(sentenceGenerator.generateSentenceFromUnigram(langaugeModel, sequence,min, max,))
        elif n == 2:
            print(sentenceGenerator.generateSentenceFromBigram(langaugeModel, sequence,version, min,  max))
        elif n == 3:
            print(sentenceGenerator.generateSentenceFromTrigram(langaugeModel, sequence, version, min,  max))

    elif choice == 3:
        # Making sure that the selected language Model is not empty
        if not checkGram(langaugeModel, n=n):
            continue
        # Asking the user to input the sequence
        print("Please insert starting sequence")
        sequence = input("Sequence: ")
        # Given the langauge model chosen above, the appropriate probability generator is called
        if n == 1:
            print("Probability = ", sequenceProbability.sequenceProbabilityFromUnigram(langaugeModel,sequence))
        elif n == 2:
            print("Probability = ", sequenceProbability.sequenceProbabilityFromBigram(langaugeModel,sequence,version))
        elif n == 3:
            print("Probability = ", sequenceProbability.sequenceProbabilityFromTrigram(langaugeModel,sequence,version))

    elif choice == 4:
        # Making sure that the selected language Model is not empty
        if not checkGram(langaugeModel, n=n):
            continue
        # Running the sequence Probability on every sentence in the testing files
        TestingFileProbability(corpusDirectory,getLM,langaugeModel,n,version)

    elif choice == 5:
        break

    else:
        print("Invalid Input")
