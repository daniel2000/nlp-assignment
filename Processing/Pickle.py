# Code adapted from
# https://docs.python.org/3/library/pickle.html
# https://www.reddit.com/r/learnpython/comments/3db16f/json_vs_pickle_breakdown/

import pickle

def saveDictToPickle(directory, fileName, dict):
    # Opening the file and dumping the contents of dictionary into the file
    file = open(directory + "//" + fileName + ".pkl", "wb")
    pickle.dump(dict, file, protocol=pickle.HIGHEST_PROTOCOL)

def readDictFromPickle(directory, fileName):
    # Checking if the fileName has '.pkl' included
    if not fileName.endswith(".pkl"):
        fileName = fileName + ".pkl"

    # Trying to open the file and if successful, it returns its contents
    try:
        file = open(directory + "//" + fileName, "rb")
    except FileNotFoundError:
        # If the file is not found
        print(fileName + " not found in " + directory)
        return False

    return pickle.load(file)

