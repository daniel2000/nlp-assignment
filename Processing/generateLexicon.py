# Code adapted from
# https://towardsdatascience.com/processing-xml-in-python-elementtree-c8992941efd2
# https://stackoverflow.com/questions/10377998/how-can-i-iterate-over-files-in-a-given-directory
import xml.etree.ElementTree as ET
import os
from Processing.Methods import *
from Processing.Pickle import *

# Getting all the files in the respective directory
directory = "..//corpusTexts"


files = []
# Going through each file in the directory given
for filename in os.listdir(directory):
    # Checking if the filename has a .xml extension
    if filename.endswith(".xml"):
        print("Success")
        files.append((os.path.join(directory, filename), filename.replace(".xml", "")))
    else:
        print("Error")


lexicon = {}

# Going through each file
for file in files:
    # Opening and parsing the xml file
    tree = ET.parse(file[0])
    root = tree.getroot()

    # Going though each sentence and getting all the child tags
    # since there are instances where the w tag is inside the child tag, this has to be checked for
    # Any empty space is removed from the words using .strip()
    for sentences in root.iter('s'):
        for token in sentences:
            if token.text is None or token.tag == "corr":
                for word in token:
                    if word.text is None:
                        for wrd in word:
                            characters = wrd.text.strip()
                            addTokenToDict(characters,lexicon)
                    else:
                        characters = word.text.strip()
                        addTokenToDict(characters,lexicon)

            else:
                characters = token.text.strip()
                addTokenToDict(characters,lexicon)

saveDictToPickle("..//lexicon", "lexicon",lexicon)