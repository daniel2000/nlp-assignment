import os
import random

class SplitData:

    def __init__(self, trainingPercentage,corpusDirectory):
        self.trainingPercentage = self.formatPercentage(trainingPercentage)
        # Essentially useless since we take the remaining amount of files, instead of this percentage
        self.testingPercentage = 1 - self.trainingPercentage
        self.corpusDirectory = corpusDirectory

    # Formatting the percentage to a value between 0 and 1
    def formatPercentage(self, percentage):
        if percentage < 0:
            print("Percentage Error")
        if 0 < percentage <= 1:
            pass
        elif 1 < percentage <= 100:
            percentage = percentage / 100
        else:
            print("Percentage Error")

        return percentage

    def returnFileNames(self):
        fileCounter = 0
        fileNames = []

        # Getting all the files in the respective directory
        for filename in os.listdir(self.corpusDirectory):
            if filename.endswith(".xml"):
                fileCounter += 1
                fileNames.append(filename)
            else:
                print("Error file not xml format (maybe a folder)")

        # Generating the number of files to be used in training and testing
        # Rounding up the number of files since only a whole number of files can be used.
        trainingFileCount = round(fileCounter * self.trainingPercentage)
        testingFileCount = fileCounter - trainingFileCount

        allrandoms = []
        # Generating random numbers for choosing the testing files, since these are most likely less files
        # than the training files
        for i in range(testingFileCount):
            while True:
                randomNo = random.randrange(0, fileCounter, 1)
                # Checking if the random number was already chosen, since the same file cannot be chosen twice
                if randomNo not in allrandoms:
                    allrandoms.append(randomNo)
                    break

        trainingFiles = []
        testingFiles = []

        # Splitting the two files in two lists and returning the file names in two separate lists
        for i in range(fileCounter):
            if i in allrandoms:
                testingFiles.append(fileNames[i])
            else:
                trainingFiles.append(fileNames[i])

        return trainingFiles, testingFiles
