# Code adapted from
# https://www.tutorialspoint.com/python/os_walk.htm
# http://techs.studyhorror.com/python-copy-rename-files-i-122

from Processing.Methods import *
import shutil

directory = "..//corpus2//ota_20.500.12024_2553//2553//download//Texts"

def copyFile(srcDir, fileName):
    dstDir = "..//corpusTexts"
    src_file = os.path.join(srcDir, fileName)
    # Copying the files from the source to the destination
    shutil.copy(src_file, dstDir)

# Creating a new directory the store the corpus in
try:
    # Since the cwd is Processing, we need to go back 1 directory to create corpusTexts in the root
    os.chdir("..")
    os.mkdir("corpusTexts")
except OSError:
    print("Creation of the directory failed/ Directory Already Exists")

# Going back into the Processing directory
os.chdir("Processing")

# Going through each and every directory and file of the corpus using the function os.walk
for root, dirs, files in os.walk(directory):
    for name in files:
        # Copying the file and saving it in a new directory
        copyFile(root,name)