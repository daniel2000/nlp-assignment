import os
import datetime

def addTokenToDict(token, dict):
    # Converting the token to lower case
    token = token.lower()
    # Checking that the token is not a string made up of white space
    if token == " ":
        print("------ERROR--------")
    # If the token is already in the dictionary add 1 to its count
    if token in dict:
        dict[token] += 1
    # If it's not found in the dictionary, create it
    else:
        dict[token] = 1

def addListToDict(listOfTokens, dict):
    # Converting the inputted list into a tuple
    tupleOfTokens = tuple(listOfTokens)
    # Checking that the tuple is not empty
    if len(tupleOfTokens) == 0:
        print("------ERROR--------")
    # If the tuple is already in the dictionary add 1 to its count
    if tupleOfTokens in dict:
        dict[tupleOfTokens] += 1
    # If it's not found in the dictionary, create it
    else:
        dict[tupleOfTokens] = 1


# Method to generate Language Model directories
def generateDirectories(trainingFileNames, testingFileNames):
    # The name of the main directory is composed of the date and time to be unique every time
    path = "languageModels" + datetime.datetime.now().strftime("%d.%m.%Y--%H.%M.%S")

    # The list of sub directories that are made, one for each mode.
    try:
        os.mkdir(path)
        os.mkdir(path + "/trainingFiles")
        os.mkdir(path + "/testingFiles")
        os.mkdir(path + "/vanilla")
        os.mkdir(path + "/laplace")
        os.mkdir(path + "/unk")
        os.mkdir(path + "/linearInterpolation")
    except OSError:
        print("Creation of the directory ", path, "failed")

    # Creating two files and writing the training and testing file names to each one of them respectively
    tf = open(path + "//trainingFiles//trainingFiles.txt", "w")
    tf.write("-------TRAINING FILE NAMES-------")
    for line in trainingFileNames:
        tf.write('\n')
        tf.write(line)
    tf.close()

    tf = open(path + "//testingFiles//testingFiles.txt", "w")
    tf.write("-------TESTING FILE NAMES-------")
    for line in testingFileNames:
        tf.write('\n')
        tf.write(line)
    tf.close()

    return path

# Method to check if a dictionary is valid of not
def checkGram(gram, n=None):
    if gram == False or gram is None or len(gram) == 0:
        print("Language Model empty/invalid")
        return False

    if n != None and n == 0:
        print("Invalid ngram number")
        return False

    return True

# Method to obtain the Testing File Names when using an already generator LM
def getTestingFileNames(lmDirectory):
    # Opening the file
    with open(lmDirectory + "//testingFiles//testingFiles.txt", "r") as file:
        testingFiles = []
        # Going through each line in the file and stripping it of any escape characters
        for line in file:
            line = line.strip()
            # If the line is a file name (endswith .xml) add it to a list
            if line.endswith(".xml"):
                testingFiles.append(line)

    return testingFiles
