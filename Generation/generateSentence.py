import random

class SentenceGenerator:

    def __init__(self,lmGenerator):
        self.lmGenerator = lmGenerator


    def getUnigram(self,version):
        # Getting the unigram language model depending on the version
        if version == "vanilla":
            return self.lmGenerator.getVanillaUnigram()
        elif version == "laplace":
            return self.lmGenerator.getLaplaceUnigram()
        elif version == "unk":
            return self.lmGenerator.getUnkUnigram()
        else:
            print("ERROR IN VERSION TYPE")

    def initialChecks(self,tokens, minWords, maxWords):
        # Checking that the minimum amount of words to be generated is greater than 0 and that this value is not smaller
        # than the provided sequence
        if minWords <= 0 or minWords < len(tokens):
            print("Error in Minimum no of words")
            raise Exception("Error in Minimum no of words")
        # Checking that the sequence has some words in it
        if len(tokens) == 0:
            print("Error in input sequence")
            raise Exception("Error in input sequence")
        # Checking that if maxWords was added, this value is not samller than the minimum no of words
        if maxWords is not None and maxWords < minWords:
            print("Error in Maximum no of words")
            raise Exception("Error in Maximum no of words")
        # Returning the minimum and maximum no of words that can be generated considering the words already inputted
        if maxWords is not None:
            return minWords - len(tokens), maxWords - len(tokens)
        else:
            return minWords - len(tokens), None


    def generateSentenceFromUnigram(self, unigram, sequence, minWords, maxWords=None):
        # Splitting the input by the space separator into a list
        tokens = sequence.split(" ")

        # Doing checks on the user input
        minWords, maxWords = self.initialChecks(tokens, minWords, maxWords)

        while True:

            # Unfortunately my implementation to generate a random word requires the whole unigram to be iterated.
            # This is very computationally expensive but this ensures thats more frequently occuring words have a higher
            # chance of being chosen
            # Another less computational expensive method is shown below

            # Since the probabilities of all the unigram keys should total up to 1, a random float number
            # between 0 and 1 is generated
            # Each item in the dictionary is iterated through and its probability is added to a running total
            # Once this running total is more than or equal to the random number generated, that key is used as the next
            # word

            randomProb = random.uniform(0,1)
            prob = 0
            for key, value in unigram.items():
                prob += value[1]
                if prob >= randomProb:
                    word = key
                    break

            # Choosing a random element from the unigram
            # This doesn't give more frequently occurring words a higher probability of being chosen since the unigram
            # only has 1 key for all the word's occurances.
            #word = random.choice(list(self.unigram))

            # Checking if an ending token (either a . or a ! or ?) has been randomly found and that the minimum no of
            # words to be generated has been surpassed
            if (word == "." or word == "?" or word == "!") and minWords <= 0:
                # The token is added to the sequence and since no more words are required the function breaks
                sequence += word
                break
            # If an ending token has been found but more words need to be generated, the loop continues
            elif (word == "." or word == "?" or word == "!") and minWords > 0:
                continue
            # If a comma is found, no space is added before it
            elif word == ",":
                sequence += word
            else:
                # If either the minimum no of words hasn't been surpassed or an ending token hasn't been found,
                # A space and the token are added
                sequence += " " + word

            # Checking if the maximum no of words to generate has been surpassed, and if it has the function breaks
            if maxWords is not None and maxWords == 1:
                break

            # Reducing the count of the minimum and maximum no of words to generate by 1
            minWords -= 1
            if maxWords is not None:
                maxWords -= 1
        return sequence

    def generateSentenceFromBigram(self, bigram, sequence, version, minWords,  maxWords=None):
        # Splitting the input by the space separator into a list
        tokens = sequence.split(" ")
        # Doing checks on the user input
        self.initialChecks(tokens,minWords, maxWords)

        # Checking if the number of words entered exceeds 2 and hence if not, the bigram cannot be calculated
        if len(tokens) < 2:
            print("Input sequence too short to generate sentence using bigram")
            raise Exception("ERROR Input sequence too short")

        insertedKeys = []
        prevToken = tokens[-1].lower()
        maxKey = None
        unkKeys = []


        while True:
            maxProb = -1
            found = False
            # Going through each element of the bigram
            for key,value in bigram.items():
                # Checking if the last word in the sequence if the first word of the key in the bigram
                if prevToken == key[0]:
                    # If the key has already been inserted, another key is searched for in order to reduce the
                    # possibility of an endless loop while generating
                    if key in insertedKeys:
                        continue
                    found = True
                    # When multiple keys are found, the one with the maximum probability is used
                    if value[1] >= maxProb:
                        maxProb = value[1]
                        maxKey = key

                # Taking note of the keys, if any, having a key (<UNK>,*) where * is any word.
                # In the case that a bigram key having the same first token as the previous word in the sequence
                # is not found, one of the below found keys is used.
                if key[0] == "<UNK>" and key[1] != "<UNK>":
                    unkKeys.append(key)

            # If no suitable key is found and UNK keys are found
            if not found and len(unkKeys) != 0:
                # The UNK token is added to the sequence
                sequence += "<UNK>"
                # A random key from the unk keys is chosen given that multiple of these tokens are found
                key = random.choice(unkKeys)
                # This key is added to the insertedKeys list
                insertedKeys.append(key)
                # Setting the word in UNK key to the next word to be worked on
                maxKey = ("<UNK>", key[1])
            elif not found and len(unkKeys) == 0:
                # If a suitable key still hasn't been found
                # A word is generated and added to the sequence and set as the next word to be worked on
                tempSeq = self.generateSentenceFromUnigram(self.getUnigram(version),".",1, 2)
                tempWords = tempSeq.split(" ")
                sequence += " " + tempWords[-1]
                maxKey = [".", tempWords[-1]]

            # If an ending token is found and the minimum no of words has been generated,
            # the ending token is added to the sequence and it is returned
            elif (maxKey[1] == "." or maxKey[1] == "?" or maxKey[1] == "!") and minWords <= 0:
                sequence += maxKey[1]
                break
            # If an ending token has been found but more words need to be generated, the loop continues
            elif (maxKey[1] == "." or maxKey[1] == "?" or maxKey[1] == "!") and minWords > 0:
                continue
            # If a comma is found, no space is added before it
            elif maxKey[1] == ",":
                insertedKeys.append(maxKey)
                sequence += maxKey[1]
            # If a word is going to be inserted, the key used is added to the insertedKeys list
            # and the word is added to the sequence
            else:
                insertedKeys.append(maxKey)
                sequence += " " + maxKey[1]

            # Checking if the maximum no of words to generate has been surpassed, and if it has the function breaks
            if maxWords is not None and maxWords == 1:
                break

            # Reducing the count of the minimum and maximum no of words to generate by 1
            minWords -= 1
            if maxWords is not None:
                maxWords -= 1

            # Assigning the word to be matched in the next iteration to the last word inserted
            prevToken = maxKey[1]
        return sequence

    def generateSentenceFromTrigram(self, trigram, sequence,version, minWords, maxWords=None):
        # Splitting the input by the space separator into a list
        tokens = sequence.split(" ")
        # Doing checks on the user input
        self.initialChecks(tokens, minWords, maxWords)

        # Checking if the number of words entered exceeds 3 and hence if not, the trigram cannot be calculated
        if len(tokens) < 3:
            print("Input sequence too short to generate sentence using bigram")
            raise Exception("ERROR input sequence too short")

        prevToken1 = tokens[-1].lower()
        prevToken2 = tokens[len(tokens)-2].lower()
        maxKey = None
        maxExactKey = None
        unkKeys = []


        insertedKeys = []

        while True:
            maxProb = -1
            maxExactProb = -1
            foundExact = False
            found = False
            # Going through each trigram key and value combination
            for key, value in trigram.items():
                # Checking if a token exists where the previous 2 words in our sequence are the first two word of the trigram token.
                if prevToken2 == key[0] and prevToken1 == key[1]:
                    # Checking if token was already used before, and skipping this iteration if it was
                    if key in insertedKeys:
                        continue
                    foundExact = True
                    # Keeping tabs on the probability of each key and keep a note of the maximum one
                    if value[1] > maxExactProb:
                        maxExactProb = value[1]
                        maxExactKey = key
                # If an exact token has not been found, a token having its first word equal to the last word in the current sequence is searched for.
                # If muliple of these token are found, the one with the highest probability is used
                if not foundExact and prevToken1 == key[0]:
                    found = True
                    if value[1] > maxProb:
                        maxProb = value[1]
                        maxKey = key
                # Taking note of the keys, if any, having a key (<UNK>,*,*) where * is any word.
                # In the case that a suitable trigram key is not found, one of the below found keys is used.
                if key[0] == "<UNK>" and key[1] != "<UNK>" and key[2] != "<UNK>":
                    unkKeys.append(key)


            if foundExact:
                maxKey = maxExactKey
                maxProb = maxExactProb
                insertedKeys.append(maxKey)
                insertToken = 2
            elif found:
                insertToken = 1

            # If neither an exact key or a key having only the first word is found,
            # but a key have the UNK token is found,
            if not found and not foundExact and len(unkKeys) != 0:
                # The unk token is added to the sequence and a random key from the list of unkKeys is chosen
                sequence += "<UNK>"
                key = random.choice(unkKeys)
                # This key is added to the list of inserted keys
                # and the words to be used in the next iteration are selected

                # The second token of the trigram key selected is used as the first word for the next iteration
                # The third token of the trigram key selected is used as the second word for the next iteration

                insertedKeys.append(key)
                maxKey = ["<UNK>", key[1] , key[2]]
            # If no suitable key has been found
            elif not found and not foundExact and len(unkKeys) == 0:
                # A word is generated and added to the sequence
                tempSeq = self.generateSentenceFromUnigram(self.getUnigram(version),".",1,2)
                tempWords = tempSeq.split(" ")
                sequence += " " + tempWords[-1]

                # The words for the next iteration are selected with
                # The latest generated word being used for as the third word for the next iteration
                # And the word being previously used as the third word now used as the second word for the next iteration

                maxKey = [".", prevToken1,tempWords[-1]]

            # If an ending token is found and the minimum no of words has been generated,
            # the ending token is added to the sequence and it is returned
            elif (maxKey[insertToken] == "." or maxKey[insertToken] == "?" or maxKey[insertToken] == "!") and minWords <= 0:
                sequence += maxKey[insertToken]
                break
            # If an ending token has been found but more words need to be generated, the loop continues
            elif (maxKey[insertToken] == "." or maxKey[insertToken] == "?" or maxKey[insertToken] == "!") and minWords > 0:
                continue
            # If a comma is found, no space is added before it
            elif maxKey[insertToken] == ",":
                sequence += maxKey[insertToken]
            # Inserting a normal word, including a space before it
            else:
                sequence += " " + maxKey[insertToken]

            # Checking if the maximum no of words to generate has been surpassed, and if it has the function breaks
            if maxWords is not None and maxWords == 1:
                break

            # Reducing the count of the minimum and maximum no of words to generate by 1
            minWords -= 1
            if maxWords is not None:
                maxWords -= 1

            # Setting the tokens to be used for the next iteration
            if foundExact:
                # When an exact key is found both token are changed
                prevToken1 = maxKey[2]
                prevToken2 = maxKey[1]
            elif found:
                # When a semi exact key is found, the last token in this iteration is put as the second to last in the next iteration
                # The last token for the next iteration is selected to be the token inserted in this iteration
                prevToken2 = prevToken1
                prevToken1 = maxKey[1]
            else:
                # When UNK token is inserted or no key was matched and a random word was generated
                # The tokens are inserted as discussed before
                prevToken1 = maxKey[2]
                prevToken2 = maxKey[1]

        return sequence