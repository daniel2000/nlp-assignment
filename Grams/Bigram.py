from Grams.GenerateNGram import *

def generateBigram(lmGenerator, laplaceSmoothing):
    # Checking if laplaceSmoothing was chosen and obtaining the correct version of the unigram
    if laplaceSmoothing:
        unigram = lmGenerator.getLaplaceUnigram()
    elif not laplaceSmoothing:
        unigram = lmGenerator.getVanillaUnigram()

    # Creating an object of the class GenerateNGram()
    generate = GenerateNGram()
    # Generating the bigram
    bigram = generate.generateNGram(lmGenerator, 2)

    # Going though each token of the new created bigram
    for token in bigram:

        # Calculating the bigram model by taking
        # the count of the 2 words together divided by the count of the first word
        if laplaceSmoothing:
            # bigram[token] = list(count, P(words[1]| words[0]
            bigram[token] = [bigram[token], (bigram[token] + 1) / (unigram[token[0]][0] + len(unigram))]
        else:
            # bigram[token] = list(count, P(words[1]| words[0])
            bigram[token] = [bigram[token],bigram[token] / unigram[token[0]][0]]

        lmGenerator.vocabSize = len(unigram)
    return bigram

