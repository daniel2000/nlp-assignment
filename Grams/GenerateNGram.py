from Processing.Methods import *
import xml.etree.ElementTree as ET


class GenerateNGram:
    tempNgram = []
    totalCount = 0

    def ngramHelper(self, characters, ngramDict, n):
        if n == 1:
            # If a unigram is being creating, this can be inserted directly into the dictionary
            addTokenToDict(characters, ngramDict)
        else:
            # Adding the words to temporary list
            self.tempNgram.append(characters.lower())
            # If the list has n 'words', these can be considered to compute the ngram model on
            if len(self.tempNgram) == n:
                addListToDict(self.tempNgram, ngramDict)
                # Removing the first 'word'
                self.tempNgram.pop(0)

    def generateNGram(self, lmGenerator, n):
        ngramDict = {}
        # Going through each file in the list of files
        for fileName in lmGenerator.trainingFileNames:
            # Using ElementTree XML parser
            tree = ET.parse(lmGenerator.corpusDirectory + "//" + fileName)
            root = tree.getroot()

            # All s tags, denoting sentences are iterated through
            for sentences in root.iter('s'):
                # Each token in the sentence is iterated through
                for token in sentences:
                    # Something another tag lines within the sentence tag itself and so this has to be catered for
                    if token.text is None or token.tag == "corr":
                        # Going through each word is this sub tag
                        for word in token:
                            # Sometimes another tag lies within the inner tag itself
                            if word.text is None:
                                for wrd in word:
                                    # Removing any white space from the characters
                                    # Incrementing the total count
                                    # Calling ngramHelper to add it to the dictionary
                                    characters = wrd.text.strip()
                                    self.totalCount += 1
                                    self.ngramHelper(characters, ngramDict, n)
                            else:
                                # Removing any white space from the characters
                                # Incrementing the total count
                                # Calling ngramHelper to add it to the dictionary
                                characters = word.text.strip()
                                self.totalCount += 1
                                self.ngramHelper(characters, ngramDict, n)
                    else:
                        # Removing any white space from the characters
                        # Incrementing the total count
                        # Calling ngramHelper to add it to the dictionary
                        characters = token.text.strip()
                        self.totalCount += 1
                        self.ngramHelper(characters, ngramDict, n)

                # The ngram model is only computed between words of the same sentence and therefore for every new sentence
                # the words to be considered to compute the ngram model on, is started from the beginning
                if n != 1:
                    self.tempNgram.clear()

        return ngramDict
