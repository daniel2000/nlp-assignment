from Grams.Trigram import *
from Grams.UNK import *
from Processing.Pickle import *
from Grams.Unigram import *
from Processing.Methods import *
from Processing.SplitTrainTestData import *


class get_generateLMs():
    def __init__(self, corpusDirectory=None, trainingPercentage=None, languageModelDirectory=None):
        # If all of the parameters are entered, the system wont know if it should use the current stored model or create a new one
        if corpusDirectory is not None and trainingPercentage is not None and languageModelDirectory is not None:
            print("ERROR Choose between generation of new language models or using a stored language model")
        # When a new language model is to be generated
        elif corpusDirectory is not None and trainingPercentage is not None:
            self.canGenerate = True
            # The training and testing file names are generated with the correct percentage
            trainingFileNames, testingFileNames = SplitData(trainingPercentage,corpusDirectory).returnFileNames()
            self.trainingFileNames = trainingFileNames
            self.testingFileNames = testingFileNames
            # The language model directories are generated
            self.lmPath = generateDirectories(self.trainingFileNames,self.testingFileNames)
        # When the user want to use the stored Language Model
        elif languageModelDirectory is not None:
            self.canGenerate = False
            self.lmPath = languageModelDirectory
            # The testing file names are obtained from the txt file stored with the language Model
            self.testingFileNames = getTestingFileNames(languageModelDirectory)
        else:
            print("ERROR is object generation")


        self.corpusDirectory = corpusDirectory
        self.vocabSize = 0

        # Predetermined weights
        # Trigram Weight
        self.lambda1 = 0.6
        # Bigram Weight
        self.lambda2 = 0.3
        # Unigram Weight
        self.lambda3 = 0.1


    # Methods to get Language Models stored as files
    # First checking if the language model can be generated and it hasn't already been generated -> generate the lm
    # If the language model cannot be generated the only option is to read it from the file,
    # if it is still not found, false is returned and this needs to be handled where the original method was called.
    def getVanillaUnigram(self):
        if self.canGenerate and not readDictFromPickle(self.lmPath + "//vanilla", "vanillaUnigram"):
            print("Vanilla Unigram not found... generating...")
            return self.generateVanillaUnigram()
        else:
            return readDictFromPickle(self.lmPath + "//vanilla", "vanillaUnigram")

    def getVanillaBigram(self):
        if self.canGenerate and not readDictFromPickle(self.lmPath + "//vanilla", "vanillaBigram"):
            print("Vanilla Bigram not found... generating...")
            return self.generateVanillaBigram()
        else:
            return readDictFromPickle(self.lmPath + "//vanilla", "vanillaBigram")

    def getVanillaTrigram(self):
        if self.canGenerate and not readDictFromPickle(self.lmPath + "//vanilla", "vanillaTrigram"):
            print("Vanilla Trigram not found... generating...")
            return self.generateVanillaTrigram()
        else:
            return readDictFromPickle(self.lmPath + "//vanilla", "vanillaTrigram")

    def getLaplaceUnigram(self):
        if self.canGenerate and not readDictFromPickle(self.lmPath +"//laplace", "laplaceUnigram"):
            print("Laplace Unigram not found... generating...")
            return self.generateLaplaceUnigram()
        else:
            return readDictFromPickle(self.lmPath +"//laplace", "laplaceUnigram")

    def getLaplaceBigram(self):
        if self.canGenerate and not readDictFromPickle(self.lmPath +"//laplace", "laplaceBigram"):
            print("Laplace Bigram not found... generating...")
            return self.generateLaplaceBigram()
        else:
            return readDictFromPickle(self.lmPath + "//laplace", "laplaceBigram")

    def getLaplaceTrigram(self):
        if self.canGenerate and not readDictFromPickle(self.lmPath + "//laplace", "laplaceTrigram"):
            print("Laplace Trigram not found... generating...")
            return self.generateLaplaceTrigram()
        else:
            return readDictFromPickle(self.lmPath + "//laplace", "laplaceTrigram")

    def getUnkUnigram(self):
        if self.canGenerate and not readDictFromPickle(self.lmPath + "//unk", "unkUnigram"):
            print("UNK Unigram not found... generating...")
            return self.generateUnkUnigram()
        else:
            return readDictFromPickle(self.lmPath + "//unk", "unkUnigram")

    def getUnkBigram(self):
        if self.canGenerate and not readDictFromPickle(self.lmPath + "//unk", "unkBigram"):
            print("UNK Bigram not found... generating...")
            return self.generateUnkBigram()
        else:
            return readDictFromPickle(self.lmPath + "//unk", "unkBigram")

    def getUnkTrigram(self):
        if self.canGenerate and not readDictFromPickle(self.lmPath + "//unk", "unkTrigram"):
            print("UNK Trigram not found... generating...")
            return self.generateUnkTrigram()
        else:
            return readDictFromPickle(self.lmPath + "//unk", "unkTrigram")

    # This function doesn't need to check if canGenerate is True since,
    # if the linear interpolation LM has not been generated but the individual language models have been,
    # this shouldn't be a problem
    def getLinearInterpolation(self, version):
        if not readDictFromPickle(self.lmPath + "//linearInterpolation", version + "LinearInterpolation"):
            print("Linear Interpolation not found... generating...")
            return self.generateLinearInterpolation(version)
        else:
            return readDictFromPickle(self.lmPath + "//linearInterpolation", version + "LinearInterpolation")



    # General method for generating language Model
    def generateLanguageModel(self, ngram, versionType):
        # versionType: vanilla False; laplace True
        # Ngram: unigram 1; bigram 2; trigram 3
        if ngram == 1:
            return generateUnigram(self, versionType)
        elif ngram == 2:
            return generateBigram(self, versionType)
        elif ngram == 3:
            return generateTrigram(self, versionType)
        else:
            print("---- ERROR when generating LM----")

    # Methods for generating individual Language Models for different versions
    # After generation the new language model is saved for future use and returned to the user
    def generateVanillaUnigram(self):
        vanillaUnigram = self.generateLanguageModel(1, False)
        saveDictToPickle(self.lmPath + "//vanilla", "vanillaUnigram", vanillaUnigram)
        return vanillaUnigram

    def generateVanillaBigram(self):
        vanillaBigram = self.generateLanguageModel(2, False)
        saveDictToPickle(self.lmPath + "//vanilla", "vanillaBigram", vanillaBigram)
        return vanillaBigram

    def generateVanillaTrigram(self):
        vanillaTrigram = self.generateLanguageModel(3, False)
        saveDictToPickle(self.lmPath + "//vanilla", "vanillaTrigram", vanillaTrigram)
        return vanillaTrigram

    def generateLaplaceUnigram(self):
        laplaceUnigram = self.generateLanguageModel(1, True)
        saveDictToPickle(self.lmPath + "//laplace", "laplaceUnigram", laplaceUnigram)
        return laplaceUnigram

    def generateLaplaceBigram(self):
        laplaceBigram = self.generateLanguageModel(2, True)
        saveDictToPickle(self.lmPath + "//laplace", "laplaceBigram", laplaceBigram)
        return laplaceBigram

    def generateLaplaceTrigram(self):
        laplaceTrigram = self.generateLanguageModel(3, True)
        saveDictToPickle(self.lmPath + "//laplace", "laplaceTrigram", laplaceTrigram)
        return laplaceTrigram

    def generateUnkLM(self):
        unkUnigram, unkBigram, unkTrigram = generateUnk(self)
        saveDictToPickle(self.lmPath + "//unk", "unkUnigram", unkUnigram)
        saveDictToPickle(self.lmPath + "//unk", "unkBigram", unkBigram)
        saveDictToPickle(self.lmPath + "//unk", "unkTrigram", unkTrigram)

    # The UNK version is implemented such that the unigram, bigram and trigram are generated at the same time.
    # Hence once one of them is required, the system checks if the UNK version was previously generated,
    # If it was, the get method above would have found it in the disk,
    # If it wasn't found, it means that none of the 3 was ever generated and hence they need to be generated.
    # These generate methods are only called if the file is not found in the directory and it can be generated (i.e, not using stored LM's).
    def generateUnkUnigram(self):
        self.generateUnkLM()
        return self.getUnkUnigram()

    def generateUnkBigram(self):
        self.generateUnkLM()
        return self.getUnkBigram()

    def generateUnkTrigram(self):
        self.generateUnkLM()
        return self.getUnkTrigram()

    def generateLinearInterpolation(self, version):
        if version == "vanilla":
            unigram = self.getVanillaUnigram()
            bigram = self.getVanillaBigram()
            trigram = self.getVanillaTrigram()
        elif version == "laplace":
            unigram = self.getLaplaceUnigram()
            bigram = self.getLaplaceBigram()
            trigram = self.getLaplaceTrigram()
        elif version == "unk":
            unigram = self.getUnkUnigram()
            bigram = self.getUnkBigram()
            trigram = self.getUnkTrigram()
        else:
            print("--------Error when obtaining language models for linear Interpolation---------")
            return False

        # If the language models cannot be generated and are not found, the methods will return false
        if unigram is False or bigram is False or trigram is False:
            return False

        # The lineaer interpolation at its base is a copy of the trigram lm
        linearInterpolation = trigram.copy()

        # Going through each token in the new dict
        for token in linearInterpolation:
            bigramToken = (token[1], token[2])
            # Changing the value to a percentage of the trigram probability +
            # a percentage of the bigram probability, taking into consideration the last two word +
            # a percentage of the unigram probability, taking into consideration the last word in the key.
            linearInterpolation[token] = [linearInterpolation[token],self.lambda1 * trigram[token][1] + self.lambda2 * bigram[bigramToken][
                1] + self.lambda3 * unigram[token[2]][1]]

        # The dictionary is saved to a pickle file
        saveDictToPickle(self.lmPath + "//linearInterpolation", version + "LinearInterpolation", linearInterpolation)

        return linearInterpolation

