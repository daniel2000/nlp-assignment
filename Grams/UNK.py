def generateUnk(lmGenerator):
    # Obtaining the vanilla version of unigram, bigram, trigram
    vanillaUnigram = lmGenerator.getVanillaUnigram()
    vanillaBigram = lmGenerator.getVanillaBigram()
    vanillaTrigram = lmGenerator.getVanillaTrigram()

    # Creating a copy of the above since they will be the base of the new unk version
    unkUnigram = vanillaUnigram.copy()
    unkBigram = vanillaBigram.copy()
    unkTrigram = vanillaTrigram.copy()

    unigramTokensToRemove = []
    bigramTokensToRemove = []
    bigramTokensToAdd = []
    trigramTokensToRemove = []
    trigramTokensToAdd = []

    # Going through each word in the vocabulary (unigram)
    for unigramToken in unkUnigram:
        # Checking if its count is 1
        if unkUnigram[unigramToken][0] == 1:
            unigramTokensToRemove.append(unigramToken)

    # Going through the old unigram tokens to be removed
    for oldToken in unigramTokensToRemove:
        # If the UNK token is already created, increments its values
        if "<UNK>" in unkUnigram:
            unkUnigram["<UNK>"][0] += unkUnigram[oldToken][0]
            unkUnigram["<UNK>"][1] += unkUnigram[oldToken][1]
        # If its not already created, create it
        else:
            unkUnigram["<UNK>"] = unkUnigram[oldToken]

        # Removing the old tokens from the unigram
        unkUnigram.pop(oldToken)

    unigramTokensToRemove.clear()

    # Going through each token in the bigram model
    for bigramToken in unkBigram:
        change = False
        # Checking if both words in the bigram token can be found in the unkUnigram
        # If they cannot, this means that they have been changed to the UNK token and hence a new token is added accordingly
        # Creating a new token for such case to be inputted into the LM later
        if unkUnigram.get(bigramToken[0]) is None and unkUnigram.get(bigramToken[1]) is None :
            newToken = ("<UNK>", "<UNK>")
            change = True
        # Checking if the first word of the bigram token cannot be found in the unkUnigram
        elif unkUnigram.get(bigramToken[0]) is None:
            newToken = ("<UNK>", bigramToken[1])
            change = True
        # Checking if the second word of the bigram token cannot be found in the unkUnigram
        elif unkUnigram.get(bigramToken[1]) is None:
            newToken = (bigramToken[0], "<UNK>")
            change = True

        # If the bigram token has been changed
        # The old token and the new token are stored separately in lists for later use
        if change:
            bigramTokensToRemove.append(bigramToken)
            bigramTokensToAdd.append(newToken)

    # Going through each new and old token from their respective lists, in parallel
    for newToken, oldToken in zip(bigramTokensToAdd, bigramTokensToRemove):
        # Checking if the new token has been already added to the LM
        if newToken in unkBigram:
            unkBigram[newToken][0] += unkBigram[oldToken][0]
            unkBigram[newToken][1] += unkBigram[oldToken][1]
        # Create a new token if it is not found
        else:
            unkBigram[newToken] = unkBigram[oldToken]
        # Removing the old token from the LM
        unkBigram.pop(oldToken)

    bigramTokensToRemove.clear()
    bigramTokensToAdd.clear()

    # Going through each token in the trigram model
    for trigramToken in unkTrigram:
        newToken = []
        change = False

        # Going through each word in the trigramToken
        for word in trigramToken:
            # Checking if any of the words in the trigram key cannot be found in the unkUnigram and hence their key
            # has been changed to UNK.
            if unkUnigram.get(word) is None:
                newToken.append("<UNK>")
                change = True
            else:
                newToken.append(word)

        newToken = tuple(newToken)

        # If the trigram token has been changed
        # The old token and the new token are stored separately in lists for later use
        if change:
            trigramTokensToRemove.append(trigramToken)
            trigramTokensToAdd.append(newToken)

    # Going through the new and old tokens in parallel
    for newToken, oldToken in zip(trigramTokensToAdd, trigramTokensToRemove):
        # Checking if the newToken has already been added to the trigram LM
        if newToken in unkTrigram:
            unkTrigram[newToken][0] += unkTrigram[oldToken][0]
            unkTrigram[newToken][1] += unkTrigram[oldToken][1]
        else:
            unkTrigram[newToken] = unkTrigram[oldToken]
        # Removing the old token from the LM
        unkTrigram.pop(oldToken)

    trigramTokensToRemove.clear()
    trigramTokensToAdd.clear()



    # Going through each token in the bigram LM once again
    for bigramToken in unkBigram:
        # unkBigram[token] = list(count, P(words[1]| words[0])
        unkBigram[bigramToken] = [unkBigram[bigramToken][0], unkBigram[bigramToken][0] / unkUnigram[bigramToken[0]][0]]

    # Going through each element in the unk trigram to recalcalcute the probability given the new bigram lm
    for trigramToken in unkTrigram:
        unkBigramToken = (trigramToken[0], trigramToken[1])
        # trigram[token] = list(count, P(words[2]| words[0],words[1])
        unkTrigram[trigramToken] = [unkTrigram[trigramToken][0], unkTrigram[trigramToken][0] / unkBigram[unkBigramToken][0]]

    return unkUnigram, unkBigram, unkTrigram
