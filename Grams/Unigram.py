from Grams.GenerateNGram import *


def generateUnigram(lmGenerator, laplaceSmoothing):
    # Creating an object of the class GenerateNGram()
    generate = GenerateNGram()

    # Generating the unigram
    unigram = generate.generateNGram(lmGenerator, 1)

    # Obtaining the total count
    totalCount = generate.totalCount

    # Going though each token of the new created unigram
    for word in unigram:
        # Calculating the unigram model by taking
        # the count of the word divided by the total amount of words
        # The number of occurrences of the word is also outputted

        if laplaceSmoothing:
            # unigram[word] = list(count, P(word)
            unigram[word] = [unigram[word], (unigram[word] + 1)/ (totalCount + len(unigram))]
        else:
            # unigram[word] = list(count, P(word)
            unigram[word] = [unigram[word], unigram[word] / totalCount]

    return unigram