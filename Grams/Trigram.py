from Grams.Bigram import *

def generateTrigram(lmGenerator, laplaceSmoothing):
    # Checking if laplaceSmoothing was chosen and obtaining the correct version of the bigram
    if laplaceSmoothing:
        bigram = lmGenerator.getLaplaceBigram()
    elif not laplaceSmoothing:
        bigram = lmGenerator.getVanillaBigram()

    # Obtaining the size of the vocabulary
    vocabSize = lmGenerator.vocabSize

    # Creating an object of the class GenerateNGram()
    generate = GenerateNGram()

    # Generating the trigram
    trigram = generate.generateNGram(lmGenerator, 3)

    # Going though each token of the new created trigram
    for token in trigram:

        # Defining the bigram token to be used to calculate the probability
        bigramToken = (token[0], token[1])
        # Calculating the trigram model by taking
        # the count of the 3 words together divided by the count of the first two words together
        # The number of occurrences of the trigram is also outputted
        if laplaceSmoothing:
            # trigram[token] = list(count, P(words[2]| words[0],words[1])
            trigram[token] = [trigram[token], (trigram[token] + 1) / (bigram[bigramToken][0] + vocabSize)]
        else:
            # trigram[token] = list(count, P(words[2]| words[0],words[1])
            trigram[token] = [trigram[token], trigram[token] / bigram[bigramToken][0]]

    return trigram