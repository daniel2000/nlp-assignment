class SequenceProbability():
    def sequenceProbabilityFromUnigram(self, unigram, sequence):
        # Making sure that the sequence is all lower case
        sequence = sequence.lower()
        # Splitting the words of the sequence into a list
        words = sequence.split(" ")
        prob = 1
        # Going through each word in the sequence given
        for word in words:
            # Checking if a token with the current word exits
            if unigram.get(word) is not None:
                # If it exists multiplying the probability with the probability of that word
                prob *= unigram[word][1]
            # If the current word doens't have a token, the UNK token is searched for, and used.
            elif unigram.get("<UNK>") is not None:
                # Its probability is multiplied with the current probabiliyt
                prob *= unigram["<UNK>"][1]
            else:
                # If neither the word or the UNK key are found, the user is notified
                print("Suitable unigram token not found")
                prob *= 1

        return prob


    def sequenceProbabilityFromBigram(self, bigram, sequence,version):
        # Making sure that the sequence is all lower case
        sequence = sequence.lower()
        # Splitting the words of the sequence into a list
        words = sequence.split(" ")
        prob = 1

        # Going through each bigram in the given sequence
        for i in range(len(words)-1):
            bigramToken = (words[i],words[i+1])
            # Checking if a bigram exits with the current two words
            if bigram.get(bigramToken) is not None:
                # If it exists multiplying the probability with the probability of that word
                prob *= bigram[bigramToken][1]
                # When the UNK token is available in the bigram provided
            elif version == "unk":
                probWord1 = -1
                probWord2 = -1
                probNone = -1
                # If an exact token doesnt exist, the bigram LM is gone through in order to find
                # a token which has either of the words in the token and the UNK token,
                # If none of these are found, the double UNK token is used, providing it exists.
                for token in bigram:
                    if words[i] == token[0] and token[1] == "<UNK>":
                        probWord1= bigram[token][1]
                    if token[0] == "<UNK>" and words[i+1] == token[1]:
                        probWord2 = bigram[token][1]
                    if token[0] == "<UNK>" and token[1] == "<UNK>":
                        probNone = bigram[token][1]

                # Checking which token exists and appending its probability to the general probability
                if probWord1 == -1 and probWord2 == -1 and probNone != -1:
                    prob *= probNone
                elif probWord1 != -1 and probWord2 == -1:
                    prob *= probWord1
                elif probWord1 == -1 and probWord2 != -1:
                    prob *= probWord2
                elif probWord1 != -1 and probWord2 != -1:
                    # Taking their average if both ('word','<UNK>') and ('<UNK>','word') are found
                    prob *= (probWord1+probWord2)/2
                else:
                    # This should never be the case
                    print("<UNK> not found")

            # If neither the bigram or the unk key are found, the user is notified
            else:
                print("Suitable bigram token not found")
                prob *= 1

        return prob

    def sequenceProbabilityFromTrigram(self, trigram, sequence, version):
        # Making sure that the sequence is all lower case
        sequence = sequence.lower()
        # Splitting the words of the sequence into a list
        words = sequence.split(" ")
        prob = 1
        # Going through all the trigrams in the given sequence
        for i in range(len(words)-2):
            trigramToken = (words[i],words[i+1],words[i+2])
            # Checking if there exists a token with the 3 words from the sequence
            if trigram.get(trigramToken) is not None:
                # The probability of this trigram is multiplied with the current probability
                prob *= trigram[trigramToken][1]

            # If unk tokens are in this trigram
            elif version == "unk":
                # Creating a dictionary of possible varaibles to be used to store the probability of different trigram
                # keys

                # probWord1 means that a key with only the first word matched and the other two being the UNK tokens
                # probWord12 means that a key with the first and second tokens matched and other being the UNK token
                # porbWord13 means that a key with the first and third token matched and the other being the UNK token
                # The same applies for the others
                probs = {"probWord1": -1, "probWord12": -1, "probWord13": -1, "probWord2": -1, "probWord23": -1, "probWord3": -1, "probNone": -1}


                for token in trigram:
                    # Checking if a token in the trigram LM exits with a combination of the above words being analysed
                    # from the sequence since a token with all 3 of them doens't exist
                    # This takes into consideration that the UNK token is in the LM
                    # Another possibility is to have a token with all 3 of them being UNK.
                    # The probability of the matched token is stored in its respective variable in the dictionary

                    if token[0] == words[i] and token[1]== "<UNK>" and token[2]== "<UNK>" :
                        probs["probWord1"] = trigram[token][1]
                    if token[0] == "<UNK>" and token[1] == words[i + 1] and token[2] == "<UNK>":
                        probs["probWord2"] = trigram[token][1]
                    if token[0] == "<UNK>" and token[1] == "<UNK>" and token[2] == words[i + 2]:
                        probs["probWord3"] = trigram[token][1]
                    if token[0] == words[i] and token[1]== words[i+1] and token[2]== "<UNK>" :
                        probs["probWord12"] = trigram[token][1]
                    if token[0] == words[i] and token[1]== "<UNK>" and token[2]== words[i+2] :
                        probs["probWord13"] = trigram[token][1]
                    if token[0]== "<UNK>" and token[1] == words[i+1] and token[2]== words[i+2]:
                        probs["probWord23"] = trigram[token][1]

                    if token[0] == "<UNK>" and token[1] == "<UNK>" and token[2] == "<UNK>":
                        probs["probNone"] = trigram[token][1]

                probToAdd = 0
                counter = 0
                # Going through the probability dictionary and adding all the probabilities to a variable
                # This doesn't consider the probNone, where the token (UNK,UNK,UNK) is used,
                # since this should be used as the last resort.
                for key , value in probs.items():
                    # Checking if any of the combinations was matched
                    if key != "probNone" and value != -1:
                        probToAdd += value
                        counter += 1

                # If no combination was matched and the triple UNK token exists, its probabilty is added to the general probability
                if counter == 0 and probs["probNone"] != -1:
                    prob *= probs["probNone"]
                # If more than one combination is found, the average is taken.
                elif counter > 0:
                    # Taking the average of all the found possible key and hence their probabilities
                    prob *= probToAdd/counter
            # If neither a matching token or an UNK token was found, the use is notified
            else:
                print("Suitable trigram token not found")
                prob *= 1

        return prob

