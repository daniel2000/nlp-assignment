from Probability.sequenceProbability import *
import xml.etree.ElementTree as ET

def TestingFileProbability(corpusDirectory, lmGenerator, gram,n , version):
    # Creating an object of the SequenceProbability class
    sequenceProbability = SequenceProbability()
    # Going through each testing file
    prob0 = 0
    totalprob = 0
    for fileName in lmGenerator.testingFileNames:
        # Using ElementTree XML parser
        tree = ET.parse(corpusDirectory + "//" + fileName)
        root = tree.getroot()

        # All s tags, denoting sentences are iterated through
        for sentences in root.iter('s'):
            line = ""
            # Each token in the sentence is iterated through
            for token in sentences:
                # Sometimes another tag lines within the sentence tag itself and so this has to be catered for
                if token.text is None or token.tag == "corr":
                    # Going through each word is this sub tag
                    for word in token:
                        # Sometimes another tag lies within the inner tag itself
                        if word.text is None:
                            for wrd in word:
                                # Add the text to the line
                                line += wrd.text
                        else:
                            # Add the text to the line
                            line += word.text
                else:
                    # Add the text to the line
                    line += token.text

            # Generating the probabilities using the SequenceProabability Class and displaying it to the user
            if n == 1:
                prob = sequenceProbability.sequenceProbabilityFromUnigram(gram, line)
                print("Unigram Probability of: " + line + " = " + str(prob))
            elif n ==2:
                prob = sequenceProbability.sequenceProbabilityFromBigram(gram, line, version)
                print("Bigram Probability of: " + line + " = " + str(prob))
            elif n ==3:
                prob = sequenceProbability.sequenceProbabilityFromTrigram(gram, line, version)
                print("Trigram Probability of: " + line + " = " + str(prob))

            # If the probability is 0
            if prob == 0:
                prob0 +=1

            # Getting the number of trigramProb generated
            totalprob +=1
    print("Percentage of sentences with 0 probability = ", (prob0/totalprob)*100, "%")